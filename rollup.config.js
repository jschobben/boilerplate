import typescript from "@rollup/plugin-typescript";
import { terser } from "rollup-plugin-terser";
import replace from 'rollup-plugin-replace';

import devtest from "./environments/devtest.env.json"
import dossier from "./environments/dossier.env.json"
import minikube from "./environments/minikube.env.json"
import staging  from "./environments/staging.env.json"
import production from "./environments/prod.env.json"

const name = "myname"; // change thjs
const envs= {
    devtest,
    dossier,
    minikube,
    staging,
    production
};


export default args => ({
    input: "src/main.ts",
    output: {
        file: `dist/${args.env}/${name}.js`,
        format: "esm",
    },
    plugins: [
        typescript({
            tsconfig: "tsconfig.json",
            module: "esnext"
        }),
        terser({
            compress: true
        }),
        replace({
            exclude: 'node_modules/**',
            delimiters: ['<@', '@>'],
            values: envs[args.env]
        })
    ]
})
