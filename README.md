# boilerplate
this project contains changes a default for projects. it contains

- typescript
- jest for testing
- a devserver (with hot reloading)
- eslint
- rollup (with typescript configured)
- environment vars


## getting started
1. change name of project in package.json
2. change name in rollup.config,js
3. change name of NAME in bitbucket pipeline to the same name as use in rollup.config.
4. npm run watch
5. npm run serve (in other terminal)
6. happy coding

## rollup
rollup takes care of: 
- typescript transpiling
- minifying code,
- inject environment variabels

### Environment variabele. 
#### Adding en variable:
1. add it to the json files in the environments folder

#### using env variabele
1. use your var anywhere with <@ prefix an @> suffix. <@MY_ENV_VAR@>
