module.exports = {
    testURL: "http://work.fake.regas.nl",
    setupFiles: [
        "./jest.setup.js"
    ],
    globals: {
        'ts-jest': {
            tsConfig: '<rootDir>/tsconfig.spec.json',
        },
    },
    transform: {
        "^.+\\.ts$": "ts-jest"
    },
    testPathIgnorePatterns: ['<rootDir>/node_modules/', '<rootDir>/dist/'],
    coveragePathIgnorePatterns: ['.stub.ts'],
    coverageThreshold: {
        global: {
            branches: 80,
            lines: 80,
        },
    },
};
