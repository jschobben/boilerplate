module.exports = {
    extends: ["./node_modules/regas-settings/.eslintrc.js"],  
    rules: {
        "@typescript-eslint/no-non-null-assertion": "off"
    },
    ignorePatterns: [
        "**/*.spec.ts"
    ]
};